# Kubernetes Setup CentOS 8

Instructions followed from [here](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#what-s-next)   
Other links provided as I had to deviate.

## Letting IP Tables see bridged traffic

Switch to root user

`sudo su`

Then create a config file with the necessary switches and apply  

```bash
cat <<EOF > /etc/sysctl.d/k8s.conf  
net.bridge.bridge-nf-call-ip6tables = 1  
net.bridge.bridge-nf-call-iptables = 1  
EOF
```

Check if **br_netfilter** module is installed

`lsmod | grep br_netfilter`

If not then load it:

`modprobe br_netfilter`

## Check that required ports are available

### Contol-plane nodes

| Protocol | Direction | Port Range |                Purpose                |       Used By       |
| :------: | :-------: | :--------: | :-----------------------------------: | :-----------------: |
|   TCP    |  Inbound  |   6443*    |         Kubernetes API server         |         All         |
|   TCP    |  Inbound  | 2379-2380  | etcd server client API kube-apiserver |        etcd         |
|   TCP    |  Inbound  |   10250    |              Kubelet API              | Self, Control plane |
|   TCP    |  Inbound  |   10251    |            kube-scheduler             |        Self         |
|   TCP    |  Inbound  |   10252    |        kube-controller-manager        |        Self         |

### Worker node(s)

| Protocol | Direction | Port Range  |      Purpose       |       Used By       |
| :------: | :-------: | :---------: | :----------------: | :-----------------: |
|   TCP    |  Inbound  |    10250    |    Kubelet API     | Self, Control plane |
|   TCP    |  Inbound  | 30000-32767 | NodePort Services† |         All         |

## Install a container runtime (Docker)

### Remove any old versions

```
sudo dnf remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### Setup the docker repository

1) Install required packages. dnf-utils provides the dnf-config-manager utility, and device-mapper-persistent-data and lvm2 are required by the devicemapper storage driver.
    ```
    sudo dnf install -y dnf-utils \
    device-mapper-persistent-data \
    lvm2
    ```
2) Use the following command to set up the stable repository.
   ```
    sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
   ```

### [Install Docker Engine - Community](https://www.techrepublic.com/article/a-better-way-to-install-docker-on-centos-8/) 

1) Install the latest version of Docker Engine - Community and containerd, or go to the next step to install a specific version:
    ```bash
    # CentOS does not support newest package by default
    # this is the newest that can be installed without complaint
    sudo dnf install docker-ce-3:18.09.1-3.el7
    ```

    ```bash
    # Optional: To enable docker DNS resolution in CentOS you must disable firewalld
    #           restart to take effect.
    sudo systemctl disable firewalld
    ```
    
    > If prompted to accept the GPG key, verify that the fingerprint matches 060A 61C5 1B55 8A7F 742B 77AA C52F EB6B 621E 9F35, and if so, accept it.  

    > Docker is installed but not started. The `docker` group is created, but no users are added to the group.  
2) Start the Docker daemon
   ```
    sudo systemctl enable --now docker
   ```
3) Add your user to the docker group then logout and back in once complete
   ```
    sudo usermod -aG docker $USER
   ```
4) Now you can install the latest version of containerd.io manually from https://download.docker.com/linux/centos/7/x86_64/stable/Packages/
   ```bash
   # Latest while writing this 04/02/2020
   # Manually installing the latest containerd.io means it will not be removed automatically 
   # if you decide to remove docker-ce and will need to be upgraded manually if an upgrade is
   # needed in the future

   sudo dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
   ```
5) Now update Docker-CE to latest version
   ```bash
   sudo dnf upgrade docker-ce
   ```
6) Confirm docker is installed and running
   ```bash
   docker run hello-world
   ```
   should see
   ```
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    1b930d010525: Pull complete
    Digest: sha256:f9dfddf63636d84ef479d645ab5885156ae030f611a56f3a7ac7f2fdd86d7e4e
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    To generate this message, Docker took the following steps:
    1. The Docker client contacted the Docker daemon.
    2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
        (amd64)
    3. The Docker daemon created a new container from that image which runs the
        executable that produces the output you are currently reading.
    4. The Docker daemon streamed that output to the Docker client, which sent it
        to your terminal.

    To try something more ambitious, you can run an Ubuntu container with:
    $ docker run -it ubuntu bash

    Share images, automate workflows, and more with a free Docker ID:
    https://hub.docker.com/

    For more examples and ideas, visit:
    https://docs.docker.com/get-started/
   ```

## Installing kubeadm, kublet, and kubectl

- **kubeadm**: the command to bootstrap the cluster.
- **kubelet**: the component that runs on all of the machines in your cluster and does things like starting pods and containers.
- **kubectl**: the command line util to talk to your cluster.

> kubeadm will not install or manage kubelet or kubectl for you, so you will need to ensure they match the version of the Kubernetes control plane you want kubeadm to install for you. If you do not, there is a risk of a version skew occurring that can lead to unexpected, buggy behaviour. However, one minor version skew between the kubelet and the control plane is supported, but the kubelet version may never exceed the API server version. For example, kubelets running 1.7.0 should be fully compatible with a 1.8.0 API server, but not vice versa.  
> 
>For information about installing kubectl, see [Install and set up kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

### Configure the kubernetes repo

Create a repository file (/etc/yum.repos.d/kubernetes.repo) with these configurations:

> Warning: These instructions exclude all Kubernetes packages from any system upgrades. This is because kubeadm and Kubernetes require special attention to upgrade.

For more information on version skews, see:

- Kubernetes [version and version-skew policy](https://kubernetes.io/docs/setup/release/version-skew-policy/)  
- Kubeadm-specific [version skew policy](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/#version-skew-policy)

```conf
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
```
Save your file then run the following commands

```bash
# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

sudo dnf install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

sudo systemctl enable --now kubelet
```
>Setting SELinux in permissive mode by running setenforce 0 and sed ... effectively disables it. This is required to allow containers to access the host filesystem, which is needed by pod networks for example. You have to do this until SELinux support is improved in the kubelet.

### Configure cgroup driver used by kubelet on control-plane node

When using Docker, kubeadm will automatically detect the cgroup driver for the kubelet and set it in the `/var/lib/kubelet/kubeadm-flags.env` file during runtime.

If you are using a different CRI, you have to modify the file `/etc/default/kubelet` (`/etc/sysconfig/` kubelet for CentOS, RHEL, Fedora) with your cgroup-driver value, like so:

```bash 
KUBELET_EXTRA_ARGS=--cgroup-driver=<value> 
```

This file will be used by `kubeadm init` and `kubeadm join` to source extra user defined arguments for the kubelet.

Please mind, that you **only** have to do that if the cgroup driver of your CRI is not cgroupfs, because that is the default value in the kubelet already.

Restarting the kubelet is required:

```
systemctl daemon-reload
systemctl restart kubelet
```

## [Creating a single control-plane cluster with kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)

